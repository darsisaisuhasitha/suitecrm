package SuiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ReportNG {
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	String expectedTooltip = "Additonal Details";

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/crm");
		Reporter.log("Starting Test");
	}

	@Test
	public void loginTest() throws Exception {
		WebElement username = driver.findElement(By.id("user_name"));
		WebElement password = driver.findElement(By.id("username_password"));
		username.sendKeys("admin");
		password.sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
		String Menu = "//*[@id=\"grouptab_0\"]";
		String SubMenu = "//*[@id=\"moduleTab_9_Leads\"]";
		WebElement menu = driver.findElement(By.xpath(Menu));
		WebElement submenu = driver.findElement(By.xpath(SubMenu));
		Actions action = new Actions(driver);
		action.moveToElement(menu).perform();
		Thread.sleep(2000L);
		submenu.click();
		Thread.sleep(2000L);
		driver.findElement(By.xpath("//table[@class='list view table-responsive']")).isSelected();
		for (int i = 1; i < 5; i++) {
			WebElement name = driver
					.findElement(By.xpath("//div[@class='list-view-rounded-corners']/table/tbody/tr[" + i + "]/td[3]"));
			WebElement user = driver
					.findElement(By.xpath("//div[@class='list-view-rounded-corners']/table/tbody/tr[" + i + "]/td[8]"));

			System.out.println("Name=:"+name.getText());
			System.out.println("User="+user.getText());
		}
		Reporter.log("Names printed successfully");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}
}
