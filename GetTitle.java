package SuiteCRM;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GetTitle {
    WebDriver driver;
    
    @BeforeMethod
    public void beforeMethod() {
       
        driver = new FirefoxDriver();
        
        //Open browser
        driver.get("http://alchemy.hguy.co/crm");
    }
    @Test
    public void test1() {
        //This test case will pass
        String title = driver.getTitle();
        System.out.println("Title is: " + title);
        Assert.assertEquals(title, "SuiteCRM");
        Reporter.log("Title displayed Successfully");
    }
   @AfterMethod
    public void afterMethod() {
             driver.close();
    }

}