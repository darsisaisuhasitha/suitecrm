package SuiteCRM;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CopyRight {
    WebDriver driver;
    
    @BeforeMethod
    public void beforeMethod() {
       
        driver = new FirefoxDriver();
        
        //Open browser
        driver.get("http://alchemy.hguy.co/crm");
    }
    @Test
    public void test1() {
        //This test case will pass
    	WebElement Footer = driver.findElement(By.id("admin_options"));
    	String FooterText = Footer.getText();
        System.out.println("First copyright text in the footer is: " + FooterText);
       Assert.assertEquals(FooterText, "� Supercharged by SuiteCRM");
       Reporter.log("Copyright displayed Successfully");
    }
   @AfterMethod
    public void afterMethod() {
             driver.close();
    }

}