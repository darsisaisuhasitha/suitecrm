package SuiteCRM;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class CSV {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	File file = new File("src/Resources/Leads.csv");
	ExpectedCondition<Boolean> expectation = new
            ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver driver) {
                    return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                }
            };

	@BeforeClass
	public void beforeClass() {
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();

		// Open browser
		driver.get("http://alchemy.hguy.co/crm");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test(priority=0)
	public void Login() {
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();

	}

	@Test(priority=1)
	  public void popupRead() throws InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 WebElement salesMenu = driver.findElement(By.id("grouptab_0"));
		 //instead of this Thread.sleep(100);  use below code
		 
		 WebDriverWait wait = new WebDriverWait(driver, 30);
         wait.until(expectation);
         
		 Actions builder = new Actions(driver);
		 builder
		 .moveToElement(salesMenu).build().perform();
      
		
		 WebElement leadsSubmenu=driver.findElement(By.xpath("//ul[@class='nav navbar-nav']//li[2]//ul[@class='dropdown-menu']//li[5]//a[@id='moduleTab_9_Leads']"));
		 leadsSubmenu.click();
		//Thread.sleep(10000);
		//navigate to import leads link
		 wait.until(expectation);
			builder
			 .moveToElement(driver.findElement(By.xpath("//ul//li[4]//div[@class='side-bar-action-icon']"))).click().build().perform();
			  
			
}

@Test(priority=2)
public void fileload()
{
	

WebElement Upload = driver.findElement(By.xpath("//input[@id='userfile']"));
Upload.sendKeys(file.getAbsolutePath());

	driver.findElement(By.xpath("//input[@id='gonext']")).click();
	driver.findElement(By.xpath("//input[@id='gonext']")).click();
	driver.findElement(By.xpath("//input[@id='gonext']")).click();
	driver.findElement(By.cssSelector("#importnow")).click();
	 driver.findElement(By.xpath("//input[@id='finished']")).click();
	
	
}
	@Test(priority=3)	
	public void checkfileloaded() throws InterruptedException
	
	{
		
	Thread.sleep(10000);	
	
	 for (int i=1;i<=10;i++)
	 {
			 WebElement name = driver.findElement(By.xpath("//div[@class='list-view-rounded-corners']/table/tbody/tr["+i+"]/td[3]"));
		String name1=name.getText();
			 System.out.println("Name"+ name1);
		  if(name1.equals("Allene Santucci")) { System.out.println("Name found"+ name1);
			  break; }
	 }
		 
		 
		}

	@AfterClass
	public void aferclass() {
		driver.close();
	}
	
}
