package SuiteCRM;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.Reporter;

public class Meeting {
	WebDriver driver;
	WebDriverWait wait;
	String activitiesMenu;
	String meetingValue = "Testing Batch 5";
	ExpectedCondition<Boolean> expectation = new

	ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
					.equals("complete");
		}
	};

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 30);
		// Open the browser
		driver.get("http://alchemy.hguy.co/crm");
	}

	@Test(priority = '0')
	public void findUserName() {

		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");

//		Finding the Login Button  
		driver.findElement(By.id("bigbutton")).click();

	}

	@Test(priority = '1')
	public void menuListMouseOver() {
// Find the activities list in the Home Page
		WebElement activitesValue = driver.findElement(By.id("grouptab_3"));

		System.out.println("Name of the Navigation Item:  " + activitesValue.getText());
		// Name of the Activities value passing into the string
		activitiesMenu = activitesValue.getText();

		// Defining the action class for the mouse over activity
		Actions action = new Actions(driver);
		action.moveToElement(activitesValue).click().perform();
		List<WebElement> activitiesList = driver.findElements(By.xpath("//li[5]//span[2]//ul[1]"));

		// ul[@class='dropdown-menu']

		for (WebElement webElement : activitiesList) {

			System.out.println("List of the menu activities: " + webElement.getText());

		}

	}

	@Test(priority = '2')
	public void scheduelMeeting() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.findElement(By.id("moduleTab_9_Meetings")).click();

		driver.findElement(By.xpath("//div[contains(text(),'Schedule Meeting')]")).click();
		Thread.sleep(5000);
		// wait.until(expectation);
		// wait.until(ExpectedConditions.elementToBeSelected(By.xpath("//input[@id='name']")));

		driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Testing Batch5");
		driver.findElement(By.xpath("//input[@id='date_start_date']")).sendKeys("05/10/2020");
		driver.findElement(By.xpath("//input[@id='date_end_date']")).sendKeys("05/10/2020");
		driver.findElement(By.xpath("//textarea[@id='description']")).sendKeys("This meeting is for testing purpose");
		;
		driver.findElement(By.xpath("//input[@id='invitees_search']")).click();
		driver.findElement(By.xpath("//input[@id='invitees_search']")).click();
		driver.findElement(By.xpath("//input[@id='invitees_add_1']")).click();
		driver.findElement(By.xpath("//input[@id='invitees_add_2']")).click();
		driver.findElement(By.xpath("//input[@id='invitees_add_3']")).click();
		// wait.until(expectation);
		Thread.sleep(5000);
		driver.findElement(By.xpath(
				"//body/div[@id='bootstrap-container']/div[@id='content']/form[@id='EditView']/div[@class='buttons']/input[1]"))
				.click();

	}

	@Test(priority = '3')
	public void viewMeetingsValidatoin() throws InterruptedException {
		driver.findElement(By.xpath("//div[contains(text(),'View Meetings')]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//table[@class='list view table-responsive']"));
		WebElement subject = driver.findElement(By.xpath("/html[1]/body[1]/div[4]/div[1]/div[3]/form[2]/div[3]/table[1]/tbody[1]/tr[1]/td[4]/b[1]/a[1]"));
		String meeting = subject.getText();
		System.out.println("Name of Meeting schedule: " + meeting);
		Assert.assertEquals(meeting, "Testing Batch5");
		System.out.println("Meeting scheduled sucessfully");
		Reporter.log("Meeting scheduled sucessfully");
	}

	@AfterClass
	public void afterClass() {
		driver.close();

	}

}
