package SuiteCRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class CountingDashlets {
	static WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/crm");
		Reporter.log("Starting Test");
	}

	@Test(priority = 0)
	public void loginTest() throws Exception {
		WebElement username = driver.findElement(By.id("user_name"));
		WebElement password = driver.findElement(By.id("username_password"));
		username.sendKeys("admin");
		password.sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
		Reporter.log("Logged in Successfully");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test(priority = '1')
	public void homepage() {

		List<WebElement> dashletName = driver.findElements(By.xpath(("//td[@class='dashlet-title']//h3//span[2]")));
		System.out.println("Number of dashlet= " + dashletName.size());

		for (WebElement webElement : dashletName) {
			System.out.println(webElement.getText());

		}
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}
}
