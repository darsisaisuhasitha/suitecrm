package SuiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;

public class AdditionalInfo {
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	String expectedTooltip = "Additonal Details";

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/crm");
		Reporter.log("Starting Test");
	}

	@Test
	public void loginTest() throws Exception {
		WebElement username = driver.findElement(By.id("user_name"));
		WebElement password = driver.findElement(By.id("username_password"));
		username.sendKeys("admin");
		password.sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
		String Menu = "//*[@id=\"grouptab_0\"]";
		String SubMenu = "//*[@id=\"moduleTab_9_Leads\"]";
		WebElement menu = driver.findElement(By.xpath(Menu));
		WebElement submenu = driver.findElement(By.xpath(SubMenu));
		Actions action = new Actions(driver);
		action.moveToElement(menu).perform();
		Thread.sleep(2000L);
		submenu.click();
		Thread.sleep(2000L);
		driver.findElement(By.xpath("//table[@class='list view table-responsive']")).isSelected();
		WebElement additional = driver.findElement(By.xpath(
				"/html[1]/body[1]/div[4]/div[1]/div[3]/form[2]/div[3]/table[1]/tbody[1]/tr[1]/td[10]/span[1]/span[1]"));
		Actions action1 = new Actions(driver);
		action1.moveToElement(additional).perform();
		Thread.sleep(2000L);
		additional.click();
		Thread.sleep(2000L);
		driver.findElement(By.xpath("/html[1]/body[1]/div[4]/div[1]/div[7]/div[2]")).isSelected();
		Thread.sleep(2000L);
		String phone = driver.findElement(By.xpath("//span[@class='phone']")).getText();
		// Printing the Mobile Phone Number
		System.out.println("Mobile Number on the additional Details:  " + phone);
		Assert.assertEquals(phone, "(075) 201-9229");
		Reporter.log("Phone Number Captured successfully");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}
}
