package SuiteCRM;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GetURL {
    WebDriver driver;
    
    @BeforeMethod
    public void beforeMethod() {
       
        driver = new FirefoxDriver();
        
        //Open browser
        driver.get("http://alchemy.hguy.co/crm");
    }
    @Test
    public void test1() {
        //This test case will pass
    	String url = driver.getCurrentUrl();
        System.out.println("URL is: " + url);
        Assert.assertEquals(url, "https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
        Reporter.log("URL displayed Successfully");
    }
   @AfterMethod
    public void afterMethod() {
             driver.close();
    }

}