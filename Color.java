package SuiteCRM;

import org.testng.annotations.Test;
import junit.framework.Assert;
import org.testng.annotations.BeforeClass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Color {
	static WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/crm");
		Reporter.log("Starting Test");
	}

	@Test
	public void loginTest() throws Exception {
		WebElement username = driver.findElement(By.id("user_name"));
		WebElement password = driver.findElement(By.id("username_password"));
		username.sendKeys("admin");
		password.sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
		String color = driver.findElement(By.id("toolbar")).getCssValue("color");
		 System.out.println("Color of Navigation menu is: " + color);
		Assert.assertEquals("rgb(83, 77, 100)", color);
		Reporter.log("Color displayed successfully");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}
}
